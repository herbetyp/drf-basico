from escola.helpers import BaseTestCase


class CourseTest(BaseTestCase):
    def setUp(self):
        self.create_course()
        super().setUp()

    def test_str(self):
        self.assertEqual(str(self.course), self.course.title)


class AvaliationTest(BaseTestCase):
    def setUp(self):
        self.create_course()
        self.create_avaliation()
        super().setUp()

    def test_str(self):
        self.assertEqual(
            str(self.avaliation),
            f'{self.avaliation.name} avaliou o curso {self.avaliation.course} com nota {self.avaliation.note}',
        )
