from django.contrib import admin

from escola.cursos.models import Avaliation, Course


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'created_at', 'updated_at', 'active')
    list_display_links = ('title',)
    search_fields = ('id', 'avaliations__name')


@admin.register(Avaliation)
class AvaliationAdmin(admin.ModelAdmin):
    list_display = (
        'course',
        'name',
        'email',
        'note',
        'created_at',
        'updated_at',
        'active',
    )
    list_display_links = ('name',)
    search_fields = ('course__title', 'course_id')
