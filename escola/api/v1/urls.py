from django.urls import path

from escola.api.v1.cursos.viewsets import CourseViewSet, AvaliationViewSet


app_name = 'course'

urlpatterns = [
    path('cursos', CourseViewSet.as_view(), name='courses'),
    path('avaliacoes', AvaliationViewSet.as_view(), name='avaliations'),
]
