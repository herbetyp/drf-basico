from django.urls import reverse

from collections import OrderedDict

from escola.helpers import BaseTestCase


class TestGetCourseViewSet(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.create_course()
        self.resp = self.client.get(reverse('course:courses'))

    def test_status_code(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_content(self):
        data = [
            OrderedDict(
                [
                    ('id', self.resp.data[0].get('id')),
                    ('title', 'teste'),
                    ('url', 'http://www.teste.com'),
                    ('created_at', '2021-01-24T21:00:00-03:00'),
                    ('active', True),
                ]
            )
        ]

        self.assertEqual(self.resp.data, data)


class TestPostCourseViewSet(BaseTestCase):
    def setUp(self):
        super().setUp()
        data = {'title': 'teste_post', 'url': 'http://www.teste.com'}

        self.login()
        self.resp = self.client.post(reverse('course:courses'), data=data)

    def test_status_code(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_content(self):
        data = {
            'id': self.resp.data['id'],
            'title': 'teste_post',
            'url': 'http://www.teste.com',
            'created_at': '2021-01-24T21:00:00-03:00',
            'active': False,
        }

        self.assertEqual(self.resp.data, data)


class TestGetAvaliationViewSet(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.create_course()
        self.create_avaliation()
        self.resp = self.client.get(reverse('course:avaliations'))

    def test_status_code(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_content(self):
        data = [
            OrderedDict(
                [
                    ('id', self.resp.data[0].get('id')),
                    ('course', self.resp.data[0].get('course')),
                    ('name', 'avaliation'),
                    ('commentary', ''),
                    ('note', '8.0'),
                    ('created_at', '2021-01-24T21:00:00-03:00'),
                    ('active', True),
                ]
            )
        ]
        self.assertEqual(self.resp.data, data)


class TestPostAvaliationViewSet(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.create_course()

        data = {
            'course': self.course.id,
            'name': 'avaliation_post',
            'email': 'teste@email.com',
            'note': 9.0,
            'active': True,
        }
        self.login()
        self.resp = self.client.post(reverse('course:avaliations'), data=data)

    def test_status_code(self):
        self.assertEqual(self.resp.status_code, 201)

    def test_content(self):
        data = {
            'id': self.resp.data['id'],
            'course': self.resp.data['course'],
            'name': 'avaliation_post',
            'commentary': '',
            'note': '9.0',
            'created_at': '2021-01-24T21:00:00-03:00',
            'active': True,
        }

        self.assertEqual(self.resp.data, data)
