from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from escola.cursos.models import Avaliation, Course
from escola.api.v1.cursos.serializers import AvaliationSerializer, CourseSerializer


class CourseViewSet(APIView):
    """Todos os cursos"""

    serializer_class = CourseSerializer

    def get(self, request):
        courses = Course.objects.all()
        serializer = self.serializer_class(courses, many=True)

        return Response(serializer.data)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)


class AvaliationViewSet(APIView):
    """Todas as avaliações"""

    serializer_class = AvaliationSerializer

    def get(self, request):
        avaliations = Avaliation.objects.all()
        serializer = self.serializer_class(avaliations, many=True)

        return Response(serializer.data)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
