from rest_framework import serializers

from escola.cursos.models import Avaliation, Course


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'title', 'url', 'created_at', 'active')


class AvaliationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Avaliation
        fields = (
            'id',
            'course',
            'name',
            'email',
            'commentary',
            'note',
            'created_at',
            'active',
        )
        extra_kwargs = {'email': {'write_only': True}}
