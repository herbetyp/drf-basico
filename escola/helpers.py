from django.test import TestCase
from django.test import Client

from model_bakery import baker

from freezegun import freeze_time


@freeze_time('2021-01-25')
class BaseTestCase(TestCase):
    def setUp(self):
        self.username = 'test'
        self.password = 'test@123'
        self.client = Client()
        self.create_user()

    def create_user(self):
        self.user = baker.make('auth.User', username=self.username)
        self.user.set_password(self.password)
        self.user.save()

    def create_course(self):
        self.course = baker.make(
            'cursos.Course',
            title='teste',
            url='http://www.teste.com',
            active=True,
        )

    def create_avaliation(self):
        self.avaliation = baker.make(
            'cursos.Avaliation',
            course=self.course,
            name='avaliation',
            email='avaliation@email.com',
            note=8.00,
        )

    def login(self):
        self.client.force_login(self.user)

    def tearDown(self):
        return super().tearDown()
